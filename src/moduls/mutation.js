export function getPokemonList(data){
    return (dispatch, getState, api) => {
        handleError(api.pokemonList(dispatch, getState, data))
    }
}

export function getPokemonDetail(data){
    return (dispatch, getState, api) => {
        handleError(api.pokemonDetail(dispatch, data))
    }
}

function handleError(promise){
    promise.catch(error => {
        console.log(error);
        alert('Error', error.message);
    })
}