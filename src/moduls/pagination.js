import React, {useState, useMemo} from "react";
import {
    View,
    Button,
    Text,
    StyleSheet
} from 'react-native';

const pageNumber = (num) => {
    console.log(num)
    for (let i = 0; i > num; i++) {
        num += 1;
    }
    return num
}


const Pagination = () => {
    const [page, setPage] = useState(1);
    const calculation = useMemo(() => pageNumber(page), [page])

    const increment = () => {
        setPage((page) - 1);
    }

    const decrement = () => {
        setPage((page) + 1);
    }


    const style = StyleSheet.create({
        Text: {
            color: 'black'
        },
        View: {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-evenly'
        } 
    })
    return (
        <View style={style.View}>
            <Button style={{width: 30, height: 50}} title='Prev' onPress={increment}/>
            <Text>{calculation}</Text>
            <Button style={style.Button} title='Next' onPress={decrement}/>
        </View>
    )
}

export default Pagination;