import React, {useEffect, useState} from "react";
import {
    NativeBaseProvider,
    Pressable,
    View,
    Text,
    ImageBackround,
} from "native-base";
import { StyleSheet, Dimensions, ImageBackground } from "react-native";

const Splash = ({navigation}) => {
    const { width, height } = Dimensions.get('window');
    const [showText, setShowText] = useState(true);

    useEffect(() => {
        const interval = setInterval(() => {
            setShowText((showText) => !showText);
        }, 1000);
        return () => clearInterval(interval);
    }, [])

    const style = StyleSheet.create({
        image: {
            width: width,
            height: height,
            flexGrow: 1,
            alignItems: "center",
            justifyContent: "center",
        },
        Text: {
            fontFamily:'Pokemon',
            fontSize: 15,
            position: "absolute",
            textAlign: "center",
            paddingTop: height * 0.8,
        }
    });
    return (
        <NativeBaseProvider>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Pressable onPress={() => navigation.navigate('Login')}>
                    <ImageBackground source={require('./assets/Frame.png')} style={style.image}>
                        <Text style={[
                            style.Text,
                            {display: showText ? 'flex' : 'none'}
                        ]}>
                            Press To Play
                        </Text>
                    </ImageBackground>
            </Pressable>
            </View>
        </NativeBaseProvider>
    )
}

export default Splash;