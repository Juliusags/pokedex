import React, {useState, useEffect} from "react";
import {
    TextInput,
    StyleSheet,
    TouchableOpacity,
    Alert,
    ActivityIndicator
} from "react-native";
import {
    View,
    Text,
} from "native-base";
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import uuid from 'react-native-uuid';

const Register = ({navigation}) => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [name, setName] = useState('')
    const [loading, setLoading] = useState(false)

    const onRegisterWithRDB = async () => {
        if (name == '' || email == '' || password == '') {
            Alert.alert('Error', 'Harap isi Semua field')
            return false;
        }
        let data = {
            id: uuid.v4(),
            name: name,
            emailId: email,
            password: password,
        };
        try {
            database()
                .ref('/users/' + data.id)
                .set(data)
                .then(() => {
                    Alert.alert('Success', 'Register Successfully!');
                    setEmail('')
                    setPassword('')
                    navigation.navigate('Login')
                });
        } catch (error) {
            Alert.alert('Error', error.message)
        }

    }

    const styles = StyleSheet.create({
        inputs: {
            borderBottomColor: "white",
            color: "black",
            paddingLeft: 10,
            flex: 1
        },
        inputContainer: {
            borderRadius: 30,
            height: 48,
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: "white",
            marginBottom: 10,
            elevation: 2,
            borderColor: "red",
            borderWidth: 2,
            width: '90%'
        },
        btnText: {
            color: '#fff',
            fontSize: 14,
            marginTop: 2,
        },
        btn: {
            backgroundColor: "red",
            width: '90%',
            height: 50,
            borderRadius: 30,
            elevation: 1,
            justifyContent: 'center',
            alignItems: 'center',
        },
    })

    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <View style={[styles.inputContainer, { marginTop: 10 }]}>
            <TextInput
                    style={styles.inputs}
                    placeholder="Enter Email Id"
                    // keyboardType="number-pad"
                    underlineColorAndroid="transparent"
                    onChangeText={value => {
                        setEmail(value)
                    }}
                    value={email}
                />
            </View>
            <View style={[styles.inputContainer, { marginTop: 10 }]}>
                <TextInput
                    style={styles.inputs}
                    placeholder="Enter Password"
                    keyboardType="number-pad"
                    underlineColorAndroid="transparent"
                    onChangeText={value => {
                        setPassword(value)
                    }}
                    value={password}
                />
            </View>
            <View style={[styles.inputContainer, { marginTop: 10 }]}>
                <TextInput
                    style={styles.inputs}
                    placeholder="Enter Username"
                    keyboardType="number-pad"
                    underlineColorAndroid="transparent"
                    onChangeText={value => {
                        setName(value)
                    }}
                    value={name}
                />
            </View>
            <TouchableOpacity
                style={styles.btn}
                onPress={() => onRegisterWithRDB()}
            >
                {
                    loading ?
                        <ActivityIndicator color={COLORS.white} />
                        :
                        <Text style={styles.btnText}>Register</Text>
                }
            </TouchableOpacity>
            <View style={{ flexDirection: 'row', margin: 10 }}>
                <Text>Already Have Account ?</Text>
                <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                    <Text>Login Here!!</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}


export default Register;