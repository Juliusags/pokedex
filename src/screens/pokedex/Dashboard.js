import { Button } from "native-base";
import React, {useState, useEffect} from "react";
import {
    View,
    Text,
    FlatList,
    Platform,
    Alert,
    StyleSheet,
    TouchableOpacity,
    Image
} from 'react-native';
import Pressable from "react-native/Libraries/Components/Pressable/Pressable";
import  { useSelector, useDispatch } from 'react-redux';
import { getPokemonList } from "../../moduls/mutation";
import Pagination from "../../moduls/pagination";

const Dashboard = ({navigation}) => {
    const dispatch = useDispatch();
    const pokemonList = useSelector(state => state.pokemonList);

    useEffect(() => {
        dispatch(getPokemonList({}));
    }, [])

    const styles = StyleSheet.create({
        container: {
          flex: 1,
          justifyContent: "center",
          marginLeft: 10,
          marginRight: 10,
        },
        headerText: {
          fontSize: 20,
          textAlign: "center",
          margin: 10,
          fontWeight: "bold"
        },
        GridViewContainer: {
         flex:1,
         justifyContent: 'center',
         alignItems: 'center',
         backgroundColor: 'red',
         height: 40,
         margin: 5,
         borderRadius: 10,
      },
      GridViewTextLayout: {
         fontSize: 15,
         fontWeight: 'bold',
         justifyContent: 'center',
         color: '#fff',
         padding: 10,
       },
       row:{
        flexDirection: 'row',
        flexWrap: 'wrap',
       },
       h1: {
        fontSize: 25,
        fontWeight: "bold",
        color: "black",
        marginBottom: 20,
        marginTop: 10
       },
       image: {
        width: 20,
        height: 20,
        marginTop: 10
       }
      });

      
    return (
        <View style={styles.container}>
            <Text style={styles.h1}>Pokedex</Text>
            <FlatList
            data = {pokemonList}
            renderItem = {({item}) =>
                    <View style={styles.GridViewContainer}>
                        <View style={styles.row}>
                            <View>
                                <Image source={require('../../assets/icon.png')} style={styles.image}/>
                            </View>
                            <View>
                                <Pressable onPress={() => {navigation.navigate("Detail", {id:item.url.match(/(\d+)\/$/)[1]})}}>
                                <Text style={styles.GridViewTextLayout}>{item.name}</Text>
                                </Pressable>
                            </View>
                        </View>
                    </View>
            }
            numColumns={2}
            />
            <Pagination/>
        </View>
    )
}

export default Dashboard;