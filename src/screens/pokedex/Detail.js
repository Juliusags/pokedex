import React, {useState, useEffect} from "react";
import {
    View,
    ScrollView,
    Image,
    Text,
} from 'native-base';
import {
    StyleSheet,
    Dimensions
} from 'react-native';
import {useNavigationState} from '@react-navigation/native';
import { getPokemonDetail } from "../../moduls/mutation";
import { useSelector, useDispatch } from "react-redux";

const Detail = ({navigation}) => {
    const id = useNavigationState(state => state.routes[state.index].params.id);
    const pokemonDetail = useSelector(state => state.pokemonDetail);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getPokemonDetail({id}));
    }, [])

    const { width, height } = Dimensions.get('window');
    const style = StyleSheet.create({
        container:{
            flex: 1,
            width: width,
            height: height
        },
        content:{
            flex: 1,
            paddingBottom: 30, 
        },
        titleText:{
            color: "black",
            fontSize: 18,
            fontWeight: "bold",
            marginBottom: 5
    
        },
        card:{
            flex: 1,
            flexDirection: "column",
            alignItems: "center",
            height: 189,
            marginTop: 66
        },
        text:{
            flex: 1,
            flexDirection: "column",
            alignItems: "flex-start",
        },
        bgImage : {
            flex: 1,
            width: width,
            height: "20%",
        } 
    });
    
    return (
        <View style={style.container}>
             <ScrollView showsHorizontalScrollIndicator={false}>
                {pokemonDetail && (
             <View>
                    <View style={style.card}>
                         <Image source={{uri: pokemonDetail.sprites.front_default}}/>
                         <Text style={{color:"black",fontSize: 24, fontFamily: "Open Sans", fontWeight: "bold"}}>{pokemonDetail.name}</Text>
                         <View style={style.text}>
                             <Text style={{fontSize: 12, color:"black"}}>Height {pokemonDetail.height}</Text>
                             <Text style={{fontSize: 12, color:"black"}}>Weight {pokemonDetail.weight}</Text>
                             <Text style={{fontSize: 12, color:"black"}}>Species {pokemonDetail.species.name}</Text>
                         </View>
                     </View>
                     <View style={style.content}>
                        <Text style={style.titleText}>Type</Text>
                        <Text>grass</Text>
                     </View>
                     <View style={style.content}>
                         <Text style={style.titleText}>Ability</Text>
                         <Text>spread Water</Text>     
                     </View>
                     <View style={style.content}>
                         <Text style={style.titleText}>Moves</Text>
                        <Text>razor-wind</Text>
                     </View>
             </View>  
            )}   
             </ScrollView>
         </View>
     );
 
}

export default Detail;