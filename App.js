import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { NativeBaseProvider, Text, Pressable } from "native-base";
import Splash from "./src/SplashScreen";
import Login from "./src/screens/auth/Login";
import Register from "./src/screens/auth/Register";
import Dashboard from "./src/screens/pokedex/Dashboard";
import Detail from "./src/screens/pokedex/Detail";
import Pokebag from "./src/screens/pokedex/Pokebag";
import { Provider } from "react-redux";
import store from "./src/moduls/store";

const navigate = createStackNavigator();

const App = () => {
  return(
    <Provider store={store}>
    <NativeBaseProvider>
      <NavigationContainer>
        <navigate.Navigator initialRouteName="Splash">
          <navigate.Screen name="Splash" component={Splash} options={{headerShown: false}}/>
          <navigate.Screen name="Login" component={Login} options={{headerShown: false}}/>
          <navigate.Screen name="Register" component={Register} options={{headerShown: false}}/>
          <navigate.Screen name="Dashboard" component={Dashboard} options={({navigation}) => ({
            title : 'Pokedex',
            headerTitleAlign: 'center',
            headerRightContainerStyle: {
              paddingRight: 7
            },
            headerLeftContainerStyle: {
              paddingLeft: 7
            },
            headerRight: () => (
              <Pressable onPress={() => navigation.navigate('Pokebag')}>
                <Text>Pokebag</Text>
              </Pressable>
            ),
            headerLeft: () => (
              <Pressable onPress={() => navigation.navigate('Dashboard')}>
                <Text>Home</Text>
              </Pressable>
            )
            
          })}/>
          <navigate.Screen name="Detail" component={Detail} options={({}) => ({
            title:'Pokemon Detail',
            headerTitleAlign: 'center'
          })}/>
          <navigate.Screen name="Pokebag" component={Pokebag} options={({}) => ({
            title:'Pokebag',
            headerTitleAlign: 'center'
          })}/>
        </navigate.Navigator>
      </NavigationContainer>
    </NativeBaseProvider>
    </Provider>
  )

}

export default App;